package quicksort;

import java.util.Random;

/**
 * Quicksort
 * Pivot is picked at random
 *
 * https://www.geeksforgeeks.org/quick-sort/
 *
 * @param <T>
 */
class QuickSort<T extends Comparable<T>>  {

    private T[] numbers;

    void sort(T[] values) {


        if (values == null || values.length == 0){
            throw new IllegalArgumentException("Empty argument");
        }
        this.numbers = values;
        quickSort(0, values.length - 1);
    }

    private void quickSort(int low, int high) {
        int i = low, j = high;

        T pivot = numbers[new Random().nextInt(numbers.length)];

        /**
         *  while
         *  1. if the left number is smaller, get the next element
         *  2. if the the current value frm the right is larger that pivot get the next element from the right list
         *  3. if we have found a left value item that is larger then the pivot, exchange the values.
         */

        while (i <= j) {

            while (numbers[i].compareTo(pivot) < 0) {
                i++;
            }
            while (pivot.compareTo(numbers[j]) < 0) {
                j--;
            }
            if (i <= j) {
                exchange(i, j);
                i++;
                j--;
            }
        }

        // sort the left list, sort the right list
        if (low < j)
            quickSort(low, j);
        if (i < high)
            quickSort(i, high);
    }

    private void exchange(int i, int j) {
        T temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
    }
}
